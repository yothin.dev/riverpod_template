import 'package:dartz/dartz.dart';
import 'package:flutter_project/core/exceptions/http_exception.dart';
import 'package:flutter_project/core/manager/dio/network_service.dart';
import 'package:flutter_project/core/common/models/paginated_response.dart';
import 'package:flutter_project/core/constants/globals.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'dashbaord_data_source.g.dart';

@Riverpod(keepAlive: true)
IDashboardDatasource dashboardDataSource(Ref ref, INetworkService networkService) {
  return DashboardRemoteDatasource(networkService);
}

abstract class IDashboardDatasource {
  Future<Either<AppException, PaginatedResponse>> fetchPaginatedProducts(
      {required int skip});
  Future<Either<AppException, PaginatedResponse>> searchPaginatedProducts(
      {required int skip, required String query});
}

class DashboardRemoteDatasource extends IDashboardDatasource {
  final INetworkService networkService;
  DashboardRemoteDatasource(this.networkService);

  @override
  Future<Either<AppException, PaginatedResponse>> fetchPaginatedProducts(
      {required int skip}) async {
    final response = await networkService.get(
      '/products',
      queryParameters: {
        'skip': skip,
        'limit': PRODUCTS_PER_PAGE,
      },
    );

    return response.fold(
          (l) => Left(l),
          (r) {
        final jsonData = r.data;
        if (jsonData == null) {
          return Left(
            AppException(
              identifier: 'fetchPaginatedData',
              statusCode: 0,
              message: 'The data is not in the valid format.',
            ),
          );
        }
        final paginatedResponse =
        PaginatedResponse.fromJson(jsonData, jsonData['products'] ?? []);
        return Right(paginatedResponse);
      },
    );
  }

  @override
  Future<Either<AppException, PaginatedResponse>> searchPaginatedProducts(
      {required int skip, required String query}) async {
    final response = await networkService.get(
      '/products/search?q=$query',
      queryParameters: {
        'skip': skip,
        'limit': PRODUCTS_PER_PAGE,
      },
    );

    return response.fold(
          (l) => Left(l),
          (r) {
        final jsonData = r.data;
        if (jsonData == null) {
          return Left(
            AppException(
              identifier: 'search PaginatedData',
              statusCode: 0,
              message: 'The data is not in the valid format.',
            ),
          );
        }
        final paginatedResponse =
        PaginatedResponse.fromJson(jsonData, jsonData['products'] ?? []);
        return Right(paginatedResponse);
      },
    );
  }
}
