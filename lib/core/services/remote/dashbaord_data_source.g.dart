// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashbaord_data_source.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$dashboardDataSourceHash() =>
    r'e45b973e668d21114a80029a07676389f260cfe6';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

typedef DashboardDataSourceRef = ProviderRef<IDashboardDatasource>;

/// See also [dashboardDataSource].
@ProviderFor(dashboardDataSource)
const dashboardDataSourceProvider = DashboardDataSourceFamily();

/// See also [dashboardDataSource].
class DashboardDataSourceFamily extends Family<IDashboardDatasource> {
  /// See also [dashboardDataSource].
  const DashboardDataSourceFamily();

  /// See also [dashboardDataSource].
  DashboardDataSourceProvider call(
    INetworkService networkService,
  ) {
    return DashboardDataSourceProvider(
      networkService,
    );
  }

  @override
  DashboardDataSourceProvider getProviderOverride(
    covariant DashboardDataSourceProvider provider,
  ) {
    return call(
      provider.networkService,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'dashboardDataSourceProvider';
}

/// See also [dashboardDataSource].
class DashboardDataSourceProvider extends Provider<IDashboardDatasource> {
  /// See also [dashboardDataSource].
  DashboardDataSourceProvider(
    this.networkService,
  ) : super.internal(
          (ref) => dashboardDataSource(
            ref,
            networkService,
          ),
          from: dashboardDataSourceProvider,
          name: r'dashboardDataSourceProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$dashboardDataSourceHash,
          dependencies: DashboardDataSourceFamily._dependencies,
          allTransitiveDependencies:
              DashboardDataSourceFamily._allTransitiveDependencies,
        );

  final INetworkService networkService;

  @override
  bool operator ==(Object other) {
    return other is DashboardDataSourceProvider &&
        other.networkService == networkService;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, networkService.hashCode);

    return _SystemHash.finish(hash);
  }
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
