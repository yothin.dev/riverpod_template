import 'package:dartz/dartz.dart';
import 'package:flutter_project/core/common/models/user/user_model.dart';
import 'package:flutter_project/core/exceptions/http_exception.dart';
import 'package:flutter_project/core/services/local/storage_service.dart';
import 'package:flutter_project/core/services/local/user_data_source.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'user_repository.g.dart';

@Riverpod(keepAlive: true)
IUserRepository userRepository(Ref ref) {
  final IStorageService storageService = ref.watch(storageServiceProvider);
  final datasource = ref.watch(userDataSourceProvider(storageService));
  final repository = UserRepository(datasource);
  return repository;
}

abstract class IUserRepository {
  Future<Either<AppException, User>> fetchUser();
  Future<bool> saveUser({required User user});
  Future<bool> deleteUser();
  Future<bool> hasUser();
}

class UserRepository extends IUserRepository {
  UserRepository(this.dataSource);

  final IUserDataSource dataSource;

  @override
  Future<bool> deleteUser() {
    return dataSource.deleteUser();
  }

  @override
  Future<Either<AppException, User>> fetchUser() {
    return dataSource.fetchUser();
  }

  @override
  Future<bool> saveUser({required User user}) {
    return dataSource.saveUser(user: user);
  }

  @override
  Future<bool> hasUser() {
    return dataSource.hasUser();
  }
}
