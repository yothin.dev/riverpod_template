import 'package:flutter_project/core/common/models/user/user_model.dart';
import 'package:flutter_project/core/common/repository/authen_repository.dart';
import 'package:flutter_project/core/common/repository/user_repository.dart';
import 'package:flutter_project/core/exceptions/http_exception.dart';
import 'package:flutter_project/features/authentication/provider/state/auth_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'login_controller.g.dart';

@riverpod
class LoginController extends _$LoginController {
  @override
  AuthState build() {
    return const AuthState.initial();
  }

  Future<void> loginUser(String username, String password) async {
    final authRepository = ref.watch(authenRepositoryProvider);
    final userRepository = ref.watch(userRepositoryProvider);

    state = const AuthState.loading();

    final response = await authRepository.loginUser(
        user: User(username: username, password: password));

    state = await response.fold(
        (failure) =>  AuthState.failure(failure),
            (user) async {
          final hasSavedUser = await userRepository.saveUser(user: user);

          if (hasSavedUser) {
             return const AuthState.success();
          }
           return AuthState.failure(AppException(message: '', statusCode: 0, identifier: ''));
        });
  }
}
