// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'network_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$networkServiceHash() => r'bd5810d8c5280f1c1bf8af965a2a9f1f3bb8e68f';

/// See also [networkService].
@ProviderFor(networkService)
final networkServiceProvider = Provider<INetworkService>.internal(
  networkService,
  name: r'networkServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$networkServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef NetworkServiceRef = ProviderRef<INetworkService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
