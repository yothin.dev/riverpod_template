// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authen_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authenRepositoryHash() => r'8b17617fbd315ebca5ef130f4b73f2dfb1573fd1';

/// See also [authenRepository].
@ProviderFor(authenRepository)
final authenRepositoryProvider = Provider<IAuthenticationRepository>.internal(
  authenRepository,
  name: r'authenRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$authenRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef AuthenRepositoryRef = ProviderRef<IAuthenticationRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
