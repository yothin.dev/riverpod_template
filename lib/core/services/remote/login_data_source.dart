import 'package:dartz/dartz.dart';
import 'package:flutter_project/core/common/models/user/user_model.dart';
import 'package:flutter_project/core/exceptions/http_exception.dart';
import 'package:flutter_project/core/manager/dio/network_service.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
part 'login_data_source.g.dart';

@Riverpod(keepAlive: true)
ILoginUserDataSource loginUserDataSource(Ref ref, INetworkService networkService) {
  return LoginUserRemoteDataSource(networkService) ;
}

abstract class ILoginUserDataSource {
  Future<Either<AppException, User>> loginUser({required User user});
}

class LoginUserRemoteDataSource implements ILoginUserDataSource {
  final INetworkService networkService;

  LoginUserRemoteDataSource(this.networkService);

  @override
  Future<Either<AppException, User>> loginUser({required User user}) async {
    try {
      final eitherType = await networkService.post(
        '/auth/login',
        data: user.toJson(),
      );
      return eitherType.fold(
            (exception) {
          return Left(exception);
        },
            (response) {
          final user = User.fromJson(response.data);
          // update the token for requests
          networkService.updateHeader(
            {'Authorization': user.token},
          );

          return Right(user);
        },
      );
    } catch (e) {
      return Left(
        AppException(
          message: 'Unknown error occured',
          statusCode: 1,
          identifier: '${e.toString()}\nLoginUserRemoteDataSource.loginUser',
        ),
      );
    }
  }
}
