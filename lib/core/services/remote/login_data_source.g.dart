// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_data_source.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$loginUserDataSourceHash() =>
    r'8012df23003a3b296290b35bb6388435be69df56';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

typedef LoginUserDataSourceRef = ProviderRef<ILoginUserDataSource>;

/// See also [loginUserDataSource].
@ProviderFor(loginUserDataSource)
const loginUserDataSourceProvider = LoginUserDataSourceFamily();

/// See also [loginUserDataSource].
class LoginUserDataSourceFamily extends Family<ILoginUserDataSource> {
  /// See also [loginUserDataSource].
  const LoginUserDataSourceFamily();

  /// See also [loginUserDataSource].
  LoginUserDataSourceProvider call(
    INetworkService networkService,
  ) {
    return LoginUserDataSourceProvider(
      networkService,
    );
  }

  @override
  LoginUserDataSourceProvider getProviderOverride(
    covariant LoginUserDataSourceProvider provider,
  ) {
    return call(
      provider.networkService,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'loginUserDataSourceProvider';
}

/// See also [loginUserDataSource].
class LoginUserDataSourceProvider extends Provider<ILoginUserDataSource> {
  /// See also [loginUserDataSource].
  LoginUserDataSourceProvider(
    this.networkService,
  ) : super.internal(
          (ref) => loginUserDataSource(
            ref,
            networkService,
          ),
          from: loginUserDataSourceProvider,
          name: r'loginUserDataSourceProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$loginUserDataSourceHash,
          dependencies: LoginUserDataSourceFamily._dependencies,
          allTransitiveDependencies:
              LoginUserDataSourceFamily._allTransitiveDependencies,
        );

  final INetworkService networkService;

  @override
  bool operator ==(Object other) {
    return other is LoginUserDataSourceProvider &&
        other.networkService == networkService;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, networkService.hashCode);

    return _SystemHash.finish(hash);
  }
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
