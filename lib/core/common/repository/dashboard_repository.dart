// final dashboardDatasourceProvider =
// Provider.family<DashboardDatasource, INetworkService>(
//       (_, networkService) => DashboardRemoteDatasource(networkService),
// );

import 'package:dartz/dartz.dart';
import 'package:flutter_project/core/exceptions/http_exception.dart';
import 'package:flutter_project/core/manager/dio/network_service.dart';
import 'package:flutter_project/core/services/remote/dashbaord_data_source.dart';
import 'package:flutter_project/core/common/models/paginated_response.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'dashboard_repository.g.dart';

@riverpod
IDashboardRepository dashboardRepository(Ref ref) {
  final networkService = ref.watch(networkServiceProvider);
  final datasource = ref.watch(dashboardDataSourceProvider(networkService));
  final respository = DashboardRepository(datasource);

  return respository;
}


abstract class IDashboardRepository {
  Future<Either<AppException, PaginatedResponse>> fetchProducts(
      {required int skip});
  Future<Either<AppException, PaginatedResponse>> searchProducts(
      {required int skip, required String query});
}

class DashboardRepository extends IDashboardRepository {
  final IDashboardDatasource dashboardDatasource;
  DashboardRepository(this.dashboardDatasource);

  @override
  Future<Either<AppException, PaginatedResponse>> fetchProducts(
      {required int skip}) {
    return dashboardDatasource.fetchPaginatedProducts(skip: skip);
  }

  @override
  Future<Either<AppException, PaginatedResponse>> searchProducts(
      {required int skip, required String query}) {
    return dashboardDatasource.searchPaginatedProducts(
        skip: skip, query: query);
  }
}

