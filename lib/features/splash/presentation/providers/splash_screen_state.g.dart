// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'splash_screen_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userLoginHash() => r'17f8f5e52d9758724a2d5f1cd1bf6b4d53293413';

/// See also [userLogin].
@ProviderFor(userLogin)
final userLoginProvider = AutoDisposeFutureProvider<bool>.internal(
  userLogin,
  name: r'userLoginProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$userLoginHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef UserLoginRef = AutoDisposeFutureProviderRef<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
