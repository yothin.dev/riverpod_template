import 'package:flutter_project/core/common/models/user/user_model.dart';
import 'package:flutter_project/core/common/repository/user_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'dashboard_state.g.dart';

@riverpod
Future<User?> currentUser(Ref ref) async {
  final response = await ref.watch(userRepositoryProvider).fetchUser();
  return response.fold((error) =>  null, (user) =>  user );
}