import 'package:dartz/dartz.dart';
import 'package:flutter_project/core/common/models/user/user_model.dart';
import 'package:flutter_project/core/exceptions/http_exception.dart';
import 'package:flutter_project/core/manager/dio/network_service.dart';
import 'package:flutter_project/core/services/remote/login_data_source.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
part 'authen_repository.g.dart';

@Riverpod(keepAlive: true)
IAuthenticationRepository authenRepository(Ref ref) {
  final INetworkService networkService = ref.watch(networkServiceProvider);
  final ILoginUserDataSource dataSource = ref.watch(loginUserDataSourceProvider(networkService));
  return AuthenticationRepository(dataSource);
}

abstract class IAuthenticationRepository {
  Future<Either<AppException, User>> loginUser({required User user});
}

class AuthenticationRepository extends IAuthenticationRepository {
  final ILoginUserDataSource dataSource;

  AuthenticationRepository(this.dataSource);

  @override
  Future<Either<AppException, User>> loginUser({required User user}) {
    return dataSource.loginUser(user: user);
  }
}
