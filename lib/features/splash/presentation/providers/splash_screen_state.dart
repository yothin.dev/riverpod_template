import 'package:flutter_project/core/common/repository/user_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'splash_screen_state.g.dart';

@riverpod
Future<bool> userLogin(Ref ref) async {
  return await ref.watch(userRepositoryProvider).hasUser();
}