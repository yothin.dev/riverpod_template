// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$dashboardControllerHash() =>
    r'736171c8b36e36da0753934af66c3f96b13f03b8';

/// See also [DashboardController].
@ProviderFor(DashboardController)
final dashboardControllerProvider =
    AutoDisposeNotifierProvider<DashboardController, DashboardState>.internal(
  DashboardController.new,
  name: r'dashboardControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$dashboardControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$DashboardController = AutoDisposeNotifier<DashboardState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
