// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_data_source.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userDataSourceHash() => r'595b1f12c87683796e93b6dedfd75651a09bb08a';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

typedef UserDataSourceRef = ProviderRef<IUserDataSource>;

/// See also [userDataSource].
@ProviderFor(userDataSource)
const userDataSourceProvider = UserDataSourceFamily();

/// See also [userDataSource].
class UserDataSourceFamily extends Family<IUserDataSource> {
  /// See also [userDataSource].
  const UserDataSourceFamily();

  /// See also [userDataSource].
  UserDataSourceProvider call(
    IStorageService service,
  ) {
    return UserDataSourceProvider(
      service,
    );
  }

  @override
  UserDataSourceProvider getProviderOverride(
    covariant UserDataSourceProvider provider,
  ) {
    return call(
      provider.service,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'userDataSourceProvider';
}

/// See also [userDataSource].
class UserDataSourceProvider extends Provider<IUserDataSource> {
  /// See also [userDataSource].
  UserDataSourceProvider(
    this.service,
  ) : super.internal(
          (ref) => userDataSource(
            ref,
            service,
          ),
          from: userDataSourceProvider,
          name: r'userDataSourceProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$userDataSourceHash,
          dependencies: UserDataSourceFamily._dependencies,
          allTransitiveDependencies:
              UserDataSourceFamily._allTransitiveDependencies,
        );

  final IStorageService service;

  @override
  bool operator ==(Object other) {
    return other is UserDataSourceProvider && other.service == service;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, service.hashCode);

    return _SystemHash.finish(hash);
  }
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member
