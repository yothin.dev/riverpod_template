import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_project/core/common/models/user/user_model.dart';
import 'package:flutter_project/core/exceptions/http_exception.dart';
import 'package:flutter_project/core/services/local/storage_service.dart';
import 'package:flutter_project/core/constants/globals.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'user_data_source.g.dart';

@Riverpod(keepAlive: true)
IUserDataSource userDataSource(Ref ref, IStorageService service) {
  return UserLocalDatasource(service);
}

abstract class IUserDataSource {
  String get storageKey;

  Future<Either<AppException, User>> fetchUser();
  Future<bool> saveUser({required User user});
  Future<bool> deleteUser();
  Future<bool> hasUser();
}

class UserLocalDatasource extends IUserDataSource {
  UserLocalDatasource(this.stroageService);

  final IStorageService stroageService;

  @override
  String get storageKey => USER_LOCAL_STORAGE_KEY;

  @override
  Future<Either<AppException, User>> fetchUser() async {
    final data = await stroageService.get(storageKey);
    if (data == null) {
      return Left(
        AppException(
          identifier: 'UserLocalDatasource',
          statusCode: 404,
          message: 'User not found',
        ),
      );
    }
    final userJson = jsonDecode(data.toString());

    return Right(User.fromJson(userJson));
  }

  @override
  Future<bool> saveUser({required User user}) async {
    return await stroageService.set(storageKey, jsonEncode(user.toJson()));
  }

  @override
  Future<bool> deleteUser() async {
    return await stroageService.remove(storageKey);
  }

  @override
  Future<bool> hasUser() async {
    return await stroageService.has(storageKey);
  }
}
